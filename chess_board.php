<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Chess Board Design</title>
    <link type="text/css" href="chess.css" rel="stylesheet" />
</head>

<body>

<div class="chess_border">
    <h1 class="head_board">Chess Board using For Loop</h1>
    <table width="270px" cellspacing="0px" cellpadding="0px" border="1px">
        <!-- cell 270px wide (8 columns x 60px) -->
        <?php

        $myNumber = $_POST['number'];

        for($i = 1; $i <= $myNumber; $i++)
        {
            echo "<div class='board'>";
            echo "<tr>";
            for($j = 1; $j <= $myNumber; $j++)
            {
                $total=$i+$j;
                if($total%2==0)
                {
                    echo "<td height=30px width=30px bgcolor=#FFFFFF></td>";
                }
                else
                {
                    echo "<td height=30px width=30px bgcolor=#000000></td>";
                }
            }
            echo "</tr>";
            echo "</div>";
        }
        ?>
    </table>
</div>

</body>
</html>