<?php

$valid_formats = array("jpg", "png", "gif", "zip", "bmp");
$max_file_size = 1024*100; //100 kb
$path = "uploads/"; // Upload directory
$count = 0;

if(isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST"){
    // Loop $_FILES to execute all files
    foreach ($_FILES['files']['name'] as $f => $name) {
        if ($_FILES['files']['error'][$f] == 4) {
            continue; // Skip file if any error found
        }
        if ($_FILES['files']['error'][$f] == 0) {
            if ($_FILES['files']['size'][$f] > $max_file_size) {
                $message[] = "$name is too large!.";
                continue; // Skip large files
            }
            elseif( ! in_array(pathinfo($name, PATHINFO_EXTENSION), $valid_formats) ){
                $message[] = "$name is not a valid format";
                continue; // Skip invalid file formats
            }
            else{ // No error found! Move uploaded files
                if(move_uploaded_file($_FILES["files"]["tmp_name"][$f], $path.$name)) {
                    $count++; // Number of successfully uploaded files
                }
            }
        }
    }
}
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <title>Multiple File Upload</title>
    <style type="text/css">
        a{ text-decoration: none; color: #333}
        h1{ font-size: 1.9em; margin: 10px 0}
        p{ margin: 8px 0}
        *{
            margin: 0;
            padding: 0;
        }
        body{
            font: 12px Arial,Tahoma,Helvetica,FreeSans,sans-serif;
            text-transform: inherit;
            color: #333;
            background: #e7edee;
            width: 100%;
            line-height: 18px;
            padding-top:200px;
        }
        .wrap{
            width: 500px;
            margin: 15px auto;
            padding: 20px 25px;
            background:#99F;
            border: 2px solid #DBDBDB;
            border-radius: 5px;
            text-align: center;
        }
        .status{
            /*display: none;*/
            padding: 8px 35px 8px 14px;
            margin: 20px 0;
            color: #468847;
            background-color: #dff0d8;
            border-radius: 4px;
        }
        input[type="submit"] {
            cursor:pointer;
            width:30%;
            border:none;
            background:#009;
            color:#FFF;
            font-weight: bold;
            margin: 20px 0;
            padding: 10px;
            border-radius:5px;
        }
        input[type="submit"]:hover {
            background-color:#9C215A;
        }
        input[type="submit"]:active {
            box-shadow:inset 0 1px 3px rgba(0,0,0,0.5);
        }
    </style>

</head>
<body>
<div class="wrap">
    <h1>Multiple File Upload</h1>
    <?php
    # error messages
    if (isset($message)) {
        foreach ($message as $msg) {
            printf("<p class='status'>%s</p></ br>\n", $msg);
        }
    }
    # success message
    if($count !=0){
        printf("<p class='status'>%d files added successfully!</p>\n", $count);
    }
    ?>
    <br />
    <br />
    <!-- Multiple file upload html form-->
    <form action="" method="post" enctype="multipart/form-data">
        <input type="file" name="files[]" multiple accept="image/*">
        <input type="submit" value="Upload">
    </form>
</div>
</body>
</html>